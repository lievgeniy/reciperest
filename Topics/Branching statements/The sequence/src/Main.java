import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner s = new Scanner(System.in);
        int input = s.nextInt();
        List<Integer> list = new ArrayList<>(input);

        for (int i = 1; i <= input && list.size() < input; i++) {
            for (int j = 1; j <= i && list.size() < input; j++) {
                list.add(i);
            }
        }

        String output = list.stream().map(String::valueOf).collect(Collectors.joining(" "));
        System.out.println(output);
    }
}
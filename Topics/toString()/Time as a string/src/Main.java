class Time {

    private int hours;
    private int minutes;
    private int seconds;

    public Time(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    @Override
    public String toString() {
        /*
        String hours = this.hours < 10 ? "0" + this.hours : String.valueOf(this.hours);
        String minutes = this.minutes < 10 ? "0" + this.minutes : String.valueOf(this.minutes);
        String seconds = this.seconds < 10 ? "0" + this.seconds : String.valueOf(this.seconds);
        return hours + ":" + minutes + ":" + seconds;
         */
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }
}
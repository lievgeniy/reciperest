package recipes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import recipes.model.Recipe;
import recipes.service.inter.RecipeServiceInterface;

@RestController
@RequestMapping("/api/recipe")
public class RecipeController {
    private Recipe recipe = new Recipe();

    private final RecipeServiceInterface recipeService;

    @Autowired
    public RecipeController(RecipeServiceInterface recipeService) {
        this.recipeService = recipeService;
    }

    @GetMapping
    public Recipe getRecipe() {
        var test = this.recipeService.getRecipe();
        return test;
        //return this.recipe;
    }

    @PostMapping
    public Recipe createRecipe(@RequestBody Recipe recipe) {
        Recipe newRecipe = this.recipeService.createRecipe(recipe);
        return newRecipe;
    }
}

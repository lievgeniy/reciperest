package recipes.repository.Interface;

import recipes.model.Recipe;

public interface RecipeRepositoryInterface {
    Recipe getRecipe();
    Recipe addRecipe(Recipe recipe);
}

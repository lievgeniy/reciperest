package recipes.repository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import recipes.model.Recipe;
import recipes.repository.Interface.RecipeRepositoryInterface;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Repository
@Qualifier("recipeSingleRepository")
public class RecipeRepository implements RecipeRepositoryInterface {
    private Recipe recipe;

    @Override
    public Recipe getRecipe() {
        return this.recipe;
    }

    @Override
    public Recipe addRecipe(Recipe recipe) {
        this.setRecipe(recipe);
        return this.recipe;
    }
}

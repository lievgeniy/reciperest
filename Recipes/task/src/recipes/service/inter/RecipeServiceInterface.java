package recipes.service.inter;

import recipes.model.Recipe;

public interface RecipeServiceInterface {
    Recipe getRecipe();
    Recipe createRecipe(Recipe recipe);
}

package recipes.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import recipes.model.Recipe;
import recipes.repository.Interface.RecipeRepositoryInterface;
import recipes.service.inter.RecipeServiceInterface;

@Service
public class RecipeService implements RecipeServiceInterface {
    private final RecipeRepositoryInterface recipeRepository;

    @Autowired
    public RecipeService(@Qualifier("recipeSingleRepository") RecipeRepositoryInterface recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    @Override
    public Recipe getRecipe() {
        var recipe = this.recipeRepository.getRecipe();
        return recipe;
    }

    @Override
    public Recipe createRecipe(Recipe recipe) {
        return this.recipeRepository.addRecipe(recipe);
    }

}
